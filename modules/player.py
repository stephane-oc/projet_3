from modules import maze, quest, boss
import pygame, sys

quest_items = quest.Quest()
map_board = maze.Maze()
boss_interaction = boss.Boss()


# class used to move player
class Player():

	# player position in list
	PLAYER_POSITION = 209

	def __init__(self):
		pass

	# move is it valid or not (wall, bonus, boss, path)
	def valid_move(self, player_next_position):
		if map_board.MAZE[player_next_position] == map_board.BLOCKS["path"]:
			return True
		elif map_board.MAZE[player_next_position] == map_board.BLOCKS["bonus"]:
			quest_items.take_bonus()
			return True
		elif map_board.MAZE[player_next_position] == map_board.BLOCKS["boss"]:
			boss_interaction.valid_end()
			return True
		else:
			return False

	# what to do if move is not valid (used for debug)
	def error_move(self):
		pass

	# move player to next block
	def move_player(self, player_next_position):
		map_board.MAZE[self.PLAYER_POSITION] = map_board.BLOCKS["path"]
		map_board.MAZE[player_next_position] = map_board.BLOCKS["player"]
		self.PLAYER_POSITION = player_next_position
		map_board.show_maze

	# move player to left
	@property
	def move_left(self):
		player_next_position = self.PLAYER_POSITION - 1
		if self.valid_move(player_next_position):
			self.move_player(player_next_position)
		else:
			self.error_move()

	# move player to right
	@property
	def move_right(self):
		player_next_position = self.PLAYER_POSITION + 1
		if self.valid_move(player_next_position):
			self.move_player(player_next_position)
		else:
			self.error_move()

	# move player to up
	@property
	def move_up(self):
		player_next_position = self.PLAYER_POSITION - 15
		if self.valid_move(player_next_position):
			self.move_player(player_next_position)
		else:
			self.error_move()

	# move player to down
	@property
	def move_down(self):
		player_next_position = self.PLAYER_POSITION + 15
		if self.valid_move(player_next_position):
			self.move_player(player_next_position)
		else:
			self.error_move()

	# waiting press key to know what to do
	@property
	def wait_key(self):
		events = pygame.event.get()
		for event in events:
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT:
					self.move_left
				if event.key == pygame.K_RIGHT:
					self.move_right
				if event.key == pygame.K_UP:
					self.move_up
				if event.key == pygame.K_DOWN:
					self.move_down
				if event.key == pygame.K_ESCAPE:
					pygame.quit()
					sys.exit()
