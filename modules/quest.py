from modules import maze
from random import randint

BONUS_COUNT = 0


# class used to randomize items location and count picked up items
class Quest():

	BONUS_NUMBER = 8

	def __init__(self):
		pass

	# items pick up counter
	def take_bonus(self):
		global BONUS_COUNT
		BONUS_COUNT += 1

	# Randomly add bonus on map (only on "path", not on player, boss or wall)
	@property
	def initialize_bonus(self):
		map_board = maze.Maze()
		bonus_init = 0
		while bonus_init < self.BONUS_NUMBER:
			rand_block = randint(0, 224)
			if map_board.MAZE[rand_block] == map_board.BLOCKS["path"]:
				map_board.MAZE[rand_block] = map_board.BLOCKS["bonus"]
				bonus_init += 1
			else:
				pass
		map_board.show_maze
