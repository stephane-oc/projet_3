from modules import quest, maze
import pygame, sys

quest_items = quest.Quest()
map_board = maze.Maze()
FINISH = 0


# class used for interaction with boss
class Boss():

	def __init__(self):
		pass

	# verify if all items are picked up
	def valid_end(self):
		if quest.BONUS_COUNT == quest_items.BONUS_NUMBER:
			self.win
		else:
			self.lose
		# games is over. stop while condition in main class
		global FINISH
		FINISH = 1

	# wait press return to quit game
	def end_game(self):
		pygame.event.clear()
		while True:
			events = pygame.event.get()
			for event in events:
				if event.type == pygame.KEYDOWN:
					if event.key == pygame.K_RETURN:
						pygame.quit()
						sys.exit()

	# what to do if you win
	@property
	def win(self):
		map_board.print_win
		self.end_game()

	# what to do if you lose
	@property
	def lose(self):
		map_board.print_lose
		self.end_game()
