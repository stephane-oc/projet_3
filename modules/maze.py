import pygame

window = pygame.display.set_mode((480, 480))


# class used for generation and print maze
class Maze():

	# hard coded maze map
	MAZE = ["0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
			"0", "1", "1", "1", "1", "1", "0", "1", "1", "1", "0", "1", "1", "1", "4",
			"0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "1", "0", "0", "0",
			"0", "1", "0", "1", "0", "1", "1", "1", "0", "1", "1", "1", "1", "1", "0",
			"0", "1", "0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "0", "0",
			"0", "1", "0", "1", "0", "1", "1", "1", "0", "1", "0", "1", "1", "1", "0",
			"0", "1", "0", "1", "0", "0", "0", "0", "0", "1", "0", "0", "0", "1", "0",
			"0", "1", "1", "1", "1", "1", "1", "1", "0", "1", "1", "1", "1", "1", "0",
			"0", "0", "0", "0", "0", "0", "0", "1", "0", "1", "0", "0", "0", "0", "0",
			"0", "1", "0", "1", "1", "1", "0", "1", "0", "1", "1", "1", "1", "1", "0",
			"0", "1", "0", "0", "0", "1", "0", "1", "0", "0", "0", "0", "0", "1", "0",
			"0", "1", "1", "1", "1", "1", "0", "1", "1", "1", "0", "1", "1", "1", "0",
			"0", "1", "0", "0", "0", "0", "0", "0", "0", "1", "0", "0", "0", "1", "0",
			"0", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "2",
			"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"]
	# dictionary needed to know relation between code and usage
	BLOCKS = {"wall": "0", "path": "1", "player": "2", "bonus": "3", "boss": "4"}

	def __init__(self):
		pass

	# Generate maze with MAZE list
	@property
	def show_maze(self):
		# initialize all pygame variables
		player = pygame.image.load("images/player.png").convert()
		boss = pygame.image.load("images/boss.png").convert()
		wall = pygame.image.load("images/wall.png").convert()
		bonus = pygame.image.load("images/bonus.png").convert()
		path = pygame.image.load("images/path.png").convert()
		block_x = 0
		block_y = 0
		# print maze with all MAZE values
		for block in self.MAZE:
			if (block_x == 15):
				block_x = 0
				block_y += 32
			if block == self.BLOCKS["path"]:
				window.blit(path, (block_x * 32, block_y))
			elif block == self.BLOCKS["player"]:
				window.blit(player, (block_x * 32, block_y))
			elif block == self.BLOCKS["bonus"]:
				window.blit(bonus, (block_x * 32, block_y))
			elif block == self.BLOCKS["boss"]:
				window.blit(boss, (block_x * 32, block_y))
			else:
				window.blit(wall, (block_x * 32, block_y))
			block_x += 1
		pygame.display.flip()

	# print win image on maze
	@property
	def print_win(self):
		youwin = pygame.image.load("images/youwin.jpg").convert()
		window.blit(youwin, (90, 141))
		pygame.display.flip()

	# print lose image on maze
	@property
	def print_lose(self):
		youlose = pygame.image.load("images/youlose.png").convert()
		window.blit(youlose, (90, 138))
		pygame.display.flip()
