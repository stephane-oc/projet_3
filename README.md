McGyver escape maze
========================

## About this game

This is my first dev project for OpenClassrooms degree (Python  developer).  
It use Python 3 for code and Pygame for graphic part.  

In this game you must pick up all potions in the maze and go to the boss.  
If don't have all potions, you lose.

## Installation

To install this game you must clone this repo with command like this :

```
git clone https://github.com/tuxfanou/projet_3.git
```

## Requirements

You need to have some requirements to use this game.  
For an automatic installation, you can user this command :

```
pip install -r requirements.txt
```

## Start

This command is used to launch the game :

```
python3 main.py
```

## Use

You only need to use **arrow keys** to move.  
When you go on **potion**, you will pick it up automatically.  
If you have all **potions**, you can go to **boss** and kill him :-)  
When game is over, you can close it with **"Return"** key.  
If you want to quit game at any time, you can do it with **"Esc"** key.

## Contributing

* Research open issues
* Open a new issue

If you can't contribute, wait an update in the issue and give more informations about this bug.  
If you can contribute, please read the [pull request process](https://help.github.com/articles/creating-a-pull-request/ "Pull request process on GitHub")
