# -*- coding: utf-8 -*-


from modules import quest, player, boss
import pygame


# main class
def main():
	pygame.init()
	map_quest = quest.Quest()
	player_move = player.Player()
	map_quest.initialize_bonus
	while boss.FINISH == 0:
		player_move.wait_key


# launch main class
if __name__ == "__main__":
	main()
